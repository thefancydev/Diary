CREATE 
TABLE [dbo].[User]
(Id INT PRIMARY KEY IDENTITY(1,1),
username NVARCHAR(50) NOT NULL,
firstname NVARCHAR(100) NOT NULL,
email NVARCHAR(MAX) NOT NULL,
lastname NVARCHAR(100) NOT NULL,
fullname AS firstname + ' ' + lastname,
passwordhash NVARCHAR(MAX) NOT NULL,
datecreated DATETIME NOT NULL,
lastlogin DATETIME NOT NULL,
isadmin BIT NOT NULL)