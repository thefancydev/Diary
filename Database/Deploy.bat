:reset
@echo off
SET /p DeploymentLocation="Deploy to Local, Live, or Test: "

for %%* in (../.) do SET product.name=%%~nx*

SET sql.files.directory="scripts"
SET repository.url="git@gitlab.com:roconnor/%product.name%.git"

SET /p DropCreateYesNo="Drop and create the database? (Y/N): "
SET /p TransactionYesNo="Use transaction? (Y/N): "

IF /I %Deploymentlocation% EQU Local (SET database.name="%product.name%.DB" && SET server.name="(local)\SQLEXPRESS") ELSE (SET database.name="%product.name%.DB-%Deploymentlocation%" && SET server.name="####")



IF /I %DropCreateYesNo% EQU Y (IF /I %TransactionYesNo% EQU Y (GOTO DropCreateTran) ELSE (GOTO DropCreate)) ELSE (IF /I %TransactionYesNo% EQU Y (GOTO Tran) ELSE (GOTO Normal))

:DropCreateTran

rh.exe /d=%database.name% /f=%sql.files.directory% /s=%server.name% /r=%repository.url% /drop
rh.exe /d=%database.name% /f=%sql.files.directory% /s=%server.name% /r=%repository.url% /t

goto:eof

:DropCreate

rh.exe /d=%database.name% /f=%sql.files.directory% /s=%server.name% /r=%repository.url% /drop
rh.exe /d=%database.name% /f=%sql.files.directory% /s=%server.name% /r=%repository.url%

goto:eof

:Tran

rh.exe /d=%database.name% /f=%sql.files.directory% /s=%server.name% /r=%repository.url% /t

goto:eof

:Normal

rh.exe /d=%database.name% /f=%sql.files.directory% /s=%server.name% /r=%repository.url%

goto:eof