﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Diary.Tests
{
    using Logic.Operations;
    [TestClass]
    public class EntryTests
    {
        public void createGlobEntryUser()
        {
            UserTests.createGlobUser();
            EntryOps.createEntry(UserOps.returnUser("foob"), "testEntry", "This is a test entry", "Top o' the world!");
        }
        [TestMethod]
        public void createDeleteEntry()
        {
            try
            {
                createGlobEntryUser();
            }
            catch
            {
                throw;
            }
            finally
            {
                EntryOps.returnEntries(
                            UserOps.returnUser("foob").Id)
                            .ToList()
                            .ForEach(x => 
                                    EntryOps.deleteEntry(x.id));
                UserTests.deleteGlobUser();
            }
        }
    }
}
