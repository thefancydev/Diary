﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Diagnostics;
using System.Linq;

namespace Diary.Tests
{
    using Ops = Logic.Operations;
    [TestClass]
    public class UserTests
    {
        public static void createGlobUser()
        {
            Ops.UserOps.createUser("foob", "foo", "bar", "barfoo", "foo@bar.com", false);
        }

        public static void deleteGlobUser()
        {
            Ops.UserOps.deleteUser("foob");
        }
        [TestMethod]
        public void createDeleteUser()
        {
            try
            {
                createGlobUser();
            }
            catch
            {
                throw;
            }
            finally
            {
                deleteGlobUser();
            }
        }

        [TestMethod]
        public void getAllUsers()
        {
            try
            {
                createGlobUser();

                Ops.UserOps
                    .getAllUsers()
                    .ToList()
                    .ForEach(x => 
                            Debug.Write(x.fullName));
                
            }
            catch
            {
                throw;
            }
            finally
            {
                deleteGlobUser();
            }
        }
    }
}
