﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Diary.Web
{
    using Logic.Types;
    public sealed class Authentication
    {
        public static User AuthenticatedAs { get; set; }
        public static bool isAuthenticated
        {
            get
            {
                if (AuthenticatedAs == null)
                    return false;
                else
                    return true; }
        }
    }
}