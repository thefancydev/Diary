﻿using System.Web;
using System.Web.Mvc;
using System.Reflection;
using System;
using System.Linq;

namespace Diary.Web
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            filters.Add(new Filters.RedirectActionFilter());
            filters.Add(new Filters.certainPermissionsFilter());
        }
    }
}
