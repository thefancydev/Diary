﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Diary.Web.Filters
{
    public class SkipAuthentication : Attribute
    {
        //! Hacky way of stopping redirect loop with Login/Register page
    }

    public class RestrictedAccess : Attribute
    {
        //! Hacky way of only allowing isAdmin roles onto certain pages
    }

    public class RedirectActionFilter : IActionFilter
    {
        public void OnActionExecuted(ActionExecutedContext filterContext)
        {
            if (filterContext.ActionDescriptor.GetCustomAttributes(typeof(SkipAuthentication), false).Any())
            {

            }
            else
            {
                if (!Authentication.isAuthenticated)
                {
                    var urlHelper = new UrlHelper(HttpContext.Current.Request.RequestContext);
                    filterContext.Result = new RedirectResult(urlHelper.Action("LoginRegister", "Account"));
                }
            }
        }

        public void OnActionExecuting(ActionExecutingContext filterContext)
        {

        }
    }

    public class certainPermissionsFilter : IActionFilter
    {
        public void OnActionExecuted(ActionExecutedContext filterContext)
        {
            if (filterContext.ActionDescriptor.GetCustomAttributes(typeof(RestrictedAccess), false).Any())
            {
                var urlHelper = new UrlHelper(HttpContext.Current.Request.RequestContext);
                if (!Authentication.isAuthenticated)
                    filterContext.Result = new RedirectResult(urlHelper.Action("LoginRegister", "Account"));
                if (!Authentication.AuthenticatedAs.isAdmin)
                {
                    filterContext.Result = new RedirectResult(urlHelper.Action("Index", "Home", new { message = "permission denied!", badAlert = true }));
                }
                else
                {
                    //! let the user through
                }
            }
            else
            {
                //! carry on the request (action isn't restricted)
            }
        }

        public void OnActionExecuting(ActionExecutingContext filterContext)
        {
            
        }
    }
}