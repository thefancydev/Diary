﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Diary.Web.Controllers
{
    using Filters;
    using Ops = Logic.Operations;
    public class AdminControlCentreController : Controller
    {
        [RestrictedAccess]
        public ActionResult adminIndex()
        {
            return View();
        }

        [RestrictedAccess]
        public ActionResult updateUser(int userId, string newUsername, string newPassword, string newfirstname, string newlastname, string newisadmin)
        {
            try
            {
                Ops.UserOps.updateUser(userId, newUsername, newPassword, newisadmin == "on" ? true : false, newfirstname, newlastname);
                TempData["message"] = $"User with username '{newUsername}' has been updated!";
                TempData["badAlert"] = false;

                return RedirectToAction("adminIndex");
            }
            catch
            {
                throw;
            }
        }

        [HttpPost]
        public ActionResult createUser(string username, string password, string email, string firstname, string lastname, bool adminrights = false)
        {
            try
            {
                Ops.UserOps.createUser(username, firstname, lastname, password, email, adminrights);
                TempData["message"] = "user has been created!";
                return RedirectToAction("adminIndex");
            }
            catch
            {
                throw;
            }
        }

        [RestrictedAccess]
        public ActionResult deleteUser(int userId)
        {
            try
            {
                Ops.UserOps.deleteUser(userId);
                TempData["message"] = "user has been deleted!";
                return RedirectToAction("adminIndex");
            }
            catch
            {
                throw;
            }
        }
    }
}