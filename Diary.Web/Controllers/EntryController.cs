﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Diary.Web.Controllers
{
    using Ops = Logic.Operations;
    public class EntryController : Controller
    {
        // GET: Entry
        public ActionResult createEntry(string message = null, bool badAlert = false)
        {
            ViewBag.message = message;
            ViewBag.badAlert = badAlert;
            return View();
        }

        [HttpPost]
        public ActionResult registerEntry(string title, string mood, string content)
        {
            try
            {
                Ops.EntryOps.createEntry(Authentication.AuthenticatedAs.Id, title, content, mood);
                return RedirectToAction("Index", "Home", new { message = "entry has been created!", badAlert = false });
            }
            catch
            {
                throw;
            }
        }

        [HttpPost]
        public ActionResult updateValues(int id, string title, string content)
        {
            Ops.EntryOps.updateEntry(id, title, content);
            return RedirectToAction("Index", "Home", new { message = "entry updated!", badAlert = false });
        }
    }
}