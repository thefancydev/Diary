﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Diary.Web.Controllers
{
    using Filters;
    using Logic.Operations;
    public class AccountController : Controller
    {
        [SkipAuthentication]
        public ActionResult LoginRegister(string message = null, bool badAlert = false)
        {
            ViewBag.message = message;
            ViewBag.badAlert = badAlert; //TODO replace ViewBag with Model
            return View();
        }
        [SkipAuthentication]
        public ActionResult logOut()
        {
            Authentication.AuthenticatedAs = null;
            return RedirectToAction("LoginRegister", new { message = "logged out successfully!" });
        }

        [HttpPost] [SkipAuthentication]
        public ActionResult checkCredentials(string username, string password)
        {
            try
            {
                if (UserOps.checkCredentials(username, password))
                {
                    Authentication.AuthenticatedAs = UserOps.returnUser(username);
                    return RedirectToAction("accountIndex");
                }
                else
                    return RedirectToAction("LoginRegister", "Account", new { message = "Invalid password!", badAlert = true });
            }
            catch
            {
                throw;
            }
        }

        public ActionResult accountIndex() => View();

        [HttpPost] [SkipAuthentication]
        public ActionResult registerUser(string firstname, string lastname, string email, string username, string password)
        {
            if (!UserOps.checkUsernameAvailability(username))
                return RedirectToAction("LoginRegister", new { message = $"the Username '{username}' exists!", badAlert = true });
            else if (string.IsNullOrWhiteSpace(firstname) || 
                        string.IsNullOrWhiteSpace(lastname) ||
                        string.IsNullOrWhiteSpace(email) ||
                        string.IsNullOrWhiteSpace(username) ||
                        string.IsNullOrWhiteSpace(password))
            {
                return RedirectToAction("LoginRegister", new { message = "Please fill in all fields", badAlert = true });
            }
            else
            {
                Authentication.AuthenticatedAs = UserOps.createUser(username, firstname, lastname, password, email);
                return RedirectToAction("accountIndex", "Account");
            }
        }
    }
}