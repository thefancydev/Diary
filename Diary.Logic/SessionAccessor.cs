﻿using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Diary.Logic
{
    public static class SessionAccessor
    {
        private static ISessionFactory SessionFactory() => 
        Fluently
            .Configure()
            .Database(MsSqlConfiguration.MsSql7
                                    .ConnectionString(s =>
                                    {
#if DEBUG
                                        s.Server(".\\SQLEXPRESS");
                                        s.Database("Diary.DB");
                                        s.TrustedConnection();
#else
                                        s.Server("####");
                                        s.Database("diarylivedb");
                                        s.Username("diarylivedb");
                                        s.Password("Di@ryDB");
#endif
                                                }))
            .Mappings(m => m.FluentMappings
                                    .AddFromAssemblyOf<Fluent.Mappings.EntryMap>()
                                    .AddFromAssemblyOf<Fluent.Mappings.UserMap>())
            .BuildConfiguration()
            .BuildSessionFactory();

        public static ISession GetCurrentSession() => SessionFactory().OpenSession();
    }
}
