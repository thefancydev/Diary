﻿using System;
using System.Linq;

namespace Diary.Logic.Operations
{
    public static class UserOps
    {
        public static Types.User createUser(string username, string firstname, string lastname, string password, string email, bool adminRights = false)
        {
            try
            {
                using (var session = SessionAccessor
                                        .GetCurrentSession())
                {
                    session.Save(new Types.User(username, firstname, lastname, password, email, adminRights));
                    session.Flush();
                    return returnUser(username);
                }
            }
            catch (Exception e)
            {
                throw new Exceptions.GenericDataWriteException($"User creation failed! {e.Message}", e);
            }
        }

        public static void updateUser(int userId, string newusername, string newpassword, bool newisadmin, string newfirstname, string newlastname)
        {
            try
            {
                using (var session = SessionAccessor
                                            .GetCurrentSession())
                {
                    var usr = session
                                        .QueryOver<Types.User>()
                                        .Where(x => x.Id == userId)
                                        .SingleOrDefault();
                    usr.userName = newusername;
                    usr.passwordHash = newpassword ==
                                            "placeholder"
                                            ? usr.passwordHash
                                            :
                                            Security
                                                .HashPassword(newpassword);
                    usr.isAdmin = newisadmin;
                    usr.firstName = newfirstname;
                    usr.lastName = newlastname;
                    session.Update(usr);
                    session.Flush();
                }
            }
            catch (Exception e)
            {
                throw new Exceptions.GenericDataWriteException($"User modification failed! {e.Message}", e);
            }
        }

        public static bool checkCredentials(string username, string password)
        {
            using (var session = SessionAccessor
                                        .GetCurrentSession())
                try
                {
                    var usr = session
                                .QueryOver<Types.User>()
                                .Where(x => x.userName == username)
                                .SingleOrDefault();
                    if (usr.passwordHash != Security.HashPassword(password))
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
                catch (Exception e)
                {
                    throw new Exceptions.GenericDataAccessException($"User doesn't exist! {e.Message}", e);
                }
        }

        public static Types.User returnUser(int userId)
        {
            using (var session = SessionAccessor
                                    .GetCurrentSession())
            {
                try
                {
                    return session.QueryOver<Types.User>()
                             .Where(x => x.Id == userId)
                             .SingleOrDefault();
                }
                catch (Exception e)
                {
                    throw new Exceptions.GenericDataAccessException($"Returning user with Id '{userId}' failed! {e.Message}", e);
                }
            }
        }

        public static bool checkUsernameAvailability(string username)
        {
            using (var session = SessionAccessor
                                        .GetCurrentSession())
                if (session
                        .QueryOver<Types.User>()
                        .Where(x => x.userName == username)
                        .RowCount() == 1)
                    return false;
                else
                    return true;
        }

        public static Types.User returnUser(string username)
        {
            using (var session = SessionAccessor
                                    .GetCurrentSession())
            {
                try
                {
                    return session.QueryOver<Types.User>()
                             .Where(x => x.userName == username)
                             .SingleOrDefault();
                }
                catch (Exception e)
                {
                    throw new Exceptions.GenericDataAccessException($"Returning user with username '{username}' failed! {e.Message}", e);
                }
            }
        }

        public static void deleteUser(int Id)
        {
            try
            {
                using (var session = SessionAccessor
                                        .GetCurrentSession())
                {
                    session.Delete(session
                                        .QueryOver<Types.User>()
                                        .Where(x => x.Id == Id)
                                        .SingleOrDefault());
                    session.Flush();
                }
            }
            catch (Exception e)
            {
                throw new Exceptions.GenericDataWriteException($"User deletion failed! {e.Message}", e);
            }
        }

        public static void deleteUser(string username)
        {
            try
            {
                using (var session = SessionAccessor
                                        .GetCurrentSession())
                {
                    session.Delete(session
                                        .QueryOver<Types.User>()
                                        .Where(x => x.userName == username)
                                        .SingleOrDefault());
                    session.Flush();
                }
            }
            catch (Exception e)
            {
                throw new Exceptions.GenericDataWriteException($"User deletion failed! {e.Message}", e);
            }
        }

        public static Types.User[] getAllUsers()
        {
            try
            {
                using (var session = SessionAccessor
                                            .GetCurrentSession())
                    return session
                                .QueryOver<Types.User>()
                                .List()
                                .ToArray();
            }
            catch (Exception e)
            {
                throw new Exceptions.GenericDataAccessException($"User retrieval failed! {e.Message}", e);
            }
        }
    }
}