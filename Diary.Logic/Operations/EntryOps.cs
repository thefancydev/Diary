﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Diary.Logic.Operations
{
    public static class EntryOps
    {
        public static void createEntry(int ownerId, string title, string content, string mood)
        {
            using (var session = SessionAccessor
                                        .GetCurrentSession())
                try
                {
                    session.Save(new Types.Entry(title, mood, content, ownerId));
                }
                catch (Exception e)
                {
                    throw new Exceptions.GenericDataWriteException($"Entry creation failed! {e.Message}", e);
                }
        }

        public static Types.Entry[] returnEntries(int owner_id)
        {
            using (var session = SessionAccessor
                                        .GetCurrentSession())
                try
                {
                    return session
                                .QueryOver<Types.Entry>()
                                .Where(x => x.owner.Id == owner_id)
                                .List()
                                .ToArray();
                }
                catch (Exception e)
                {
                    throw new Exceptions.GenericDataAccessException($"Entry reading failed! {e.Message}", e);
                }
        }

        public static void updateEntry(int Id, string newTitle, string newContent)
        {
            using (var session = SessionAccessor
                                        .GetCurrentSession())
            {
                var entry = session
                              .QueryOver<Types.Entry>()
                              .Where(x => x.id == Id)
                              .SingleOrDefault();
                entry.title = newTitle;
                entry.content = newContent;
                entry.LastEdited = entry.title == newTitle || 
                                    entry.content == newContent 
                                    ? DateTime.UtcNow 
                                    : entry.LastEdited;

                session.Update(entry);
                session.Flush();
            }
        }

        public static void deleteEntry(int Id)
        {
            using (var session = SessionAccessor
                                        .GetCurrentSession())
            {
                session.Delete(session
                                    .QueryOver<Types.Entry>()
                                    .Where(x => x.id == Id)
                                    .SingleOrDefault());
            }
        }

        public static void createEntry(Types.User owner, string title, string content, string mood)
        {
            using (var session = SessionAccessor
                                        .GetCurrentSession())
                try
                {
                    session.Save(new Types.Entry(title, mood, content, owner));
                }
                catch (Exception e)
                {
                    throw new Exceptions.GenericDataWriteException($"Entry creation failed! {e.Message}", e);
                }
        }
    }
}
