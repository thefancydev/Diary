﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;

namespace Diary.Logic.Fluent.Mappings
{
    public class EntryMap : ClassMap<Types.Entry>
    {
        public EntryMap()
        {
            Id(x => x.id).GeneratedBy.Identity().Not.Nullable();
            Map(x => x.title).Not.Nullable().Column("title");
            Map(x => x.mood).Not.Nullable().Column("mood");
            Map(x => x.content).Not.Nullable().Column("content");
            Map(x => x.archived).Not.Nullable().Column("archived");
            Map(x => x.dateCreated).Not.Nullable().Column("datecreated");
            Map(x => x.LastEdited).Not.Nullable().Column("lastedited");
            References(x => x.owner).Column("owner_id");
    }
    }
}
