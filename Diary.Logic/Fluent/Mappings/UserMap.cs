﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate;
using FluentNHibernate.Mapping;

namespace Diary.Logic.Fluent.Mappings
{
    public class UserMap : ClassMap<Types.User>
    {
        public UserMap()
        {
            Table("[User]");
            LazyLoad();
            Id(x => x.Id).GeneratedBy.Identity().Not.Nullable().Column("Id");
            Map(x => x.userName).Not.Nullable().Column("username");
            Map(x => x.email).Not.Nullable().Column("email");
            Map(x => x.firstName).Not.Nullable().Column("firstname");
            Map(x => x.lastName).Not.Nullable().Column("lastname");
            Map(x => x.fullName).ReadOnly().Column("fullname");
            Map(x => x.passwordHash).Not.Nullable().Column("passwordhash");
            Map(x => x.dateCreated).Column("datecreated");
            Map(x => x.lastLogin).Column("lastlogin");
            Map(x => x.isAdmin).Not.Nullable().Column("isadmin");
            HasMany(x => x.diaryEntries).KeyColumn("owner_id").Not.LazyLoad();
    }
    }
}
