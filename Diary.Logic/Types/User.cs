﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Diary.Logic.Types
{
    public class User
    {
        public User()
        {
            diaryEntries = new List<Entry>();
            dateCreated = 
                lastLogin = 
                DateTime.UtcNow;
        }

        public User(string username, string firstname, string lastname, string password, string eMail, bool isadmin = false)
        {
            userName = username;
            firstName = firstname;
            email = eMail;
            lastName = lastname;
            passwordHash = Security
                                .HashPassword(password);
            dateCreated =
                lastLogin =
                DateTime.UtcNow;
            isAdmin = isadmin;
            diaryEntries = new List<Entry>();
        }
        public virtual int Id { get; protected set; }
        public virtual string userName { get; set; }
        public virtual string firstName { get; set; }
        public virtual string lastName { get; set; }
        public virtual string email { get; set; }
        public virtual string fullName { get; protected set; }
        public virtual string passwordHash { get; set; }
        public virtual DateTime dateCreated { get; protected set; }
        public virtual DateTime lastLogin { get; set; }
        public virtual bool isAdmin { get; set; }
        public virtual IList<Entry> diaryEntries { get; set; }
    }
}
