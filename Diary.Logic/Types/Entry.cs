﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Diary.Logic.Types
{
    public class Entry
    {
        public Entry()
        {
            dateCreated =
                LastEdited =
                DateTime.UtcNow;
        }

        public Entry(string Title, string Mood, string Content, User Owner)
        {
            title = Title;
            mood = Mood;
            content = Content;
            owner = Owner;
            archived = false;
            dateCreated =
                LastEdited =
                DateTime.UtcNow;
        }

        public Entry(string Title, string Mood, string Content, int Owner_id)
        {
            title = Title;
            mood = Mood;
            content = Content;
            owner = Operations.UserOps.returnUser(Owner_id);
            archived = false;
            dateCreated =
                LastEdited =
                DateTime.UtcNow;
        }
        public virtual int id { get; protected set; }
        public virtual string title { get; set; }
        public virtual string mood { get; set; }
        public virtual string content { get; set; }
        public virtual User owner { get; protected set; }
        public virtual bool archived { get; set; }
        public virtual DateTime dateCreated { get; protected set; }
        public virtual DateTime LastEdited { get; set; }
    }
}
