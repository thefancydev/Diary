﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Diary.Logic.Exceptions
{
    public class GenericDataAccessException : Exception
    {
        public GenericDataAccessException()
        {

        }

        public GenericDataAccessException(string message) : base(message)
        {

        }

        public GenericDataAccessException(string message, Exception innerException) : base(message, innerException)
        {

        }
    }
}
