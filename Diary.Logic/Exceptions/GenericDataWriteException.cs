﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Diary.Logic.Exceptions
{
    public class GenericDataWriteException : Exception
    {
        public GenericDataWriteException()
        {

        }

        public GenericDataWriteException(string message) : base(message)
        {

        }

        public GenericDataWriteException(string message, Exception innerException) : base(message, innerException)
        {

        }
    }
}
